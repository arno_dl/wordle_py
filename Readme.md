# WORDLE PYTHON

jeu de wordle classique en cli avec rich cli.

## lib nécéssaires

- [rich library](<https://rich.readthedocs.io/en/stable/index.html>)
- [micro menu](<https://github.com/andli/micromenu>)
- [Requests](<https://fr.python-requests.org/en/latest/>)

## Liste de mots

Il faut au préalable s'enregistrer et demander une cle gratuite (un peu de patience)
j'ai utilisé l'api dicolink avec GET -> mot et GET -> mots
[Documentation API Dicolink](<https://www.dicolink.com/api/documentation>)

## Logs

La configuration des logs se fait dans le fichier logging.conf, dans chaque classe on peut utiliser:

```python
logging.config.fileConfig(fname='logging.conf', disable_existing_loggers=False)
logger = logging.getLogger(__name__)
```