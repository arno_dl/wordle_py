"""
Toutes les fonctions de comparaison de strings du programme sont ici.
"""

def compare(word: str, guess: str) -> tuple:
    """
    Compare une estimation au mot donné et renvoie un tuple avec un booléen et
    un dictionnaire indiquant pour chaque caractère du mot si:
    1: Il est à la même position dans le mot et l'estimation.
    -1: Le caractère comparé est présent dans l'autre mot mais pas à cet index.
    0: Le caractère comparé dans l'estimation n'est pas présent dans le mot.
    """
    word_d = {}
    guess_d = {}

    for index, letter in enumerate(word):
        word_d.setdefault(letter, []).append(index)

    for index, letter in enumerate(guess):
        guess_d.setdefault(letter, []).append(index)

    if word_d == guess_d:
        for i in guess_d.keys():
            guess_d[i] = 1
        return (True, guess_d)
    else:
        for i in guess_d.keys():
            try:
                word_d[i]
                if word_d[i] == guess_d[i]:
                    guess_d[i] = '🟩'
                else:
                    guess_d[i] = '🟨'
            except KeyError:
                guess_d[i] = '🟥'
        return (False, guess_d)



if __name__ == "__main__":
    word = input("Enter word:\n")
    guess = input("Enter guess:\n")

    print(compare(word, guess))
