OUT_DIR = './out/guessWord.txt'
SAVE_FILE = './out/default_save_file.json'

URL_WORD_REQUEST = "https://api.dicolink.com/v1/mots/motauhasard"
URL_WORDS_REQUEST = "https://api.dicolink.com/v1/mots/motsauhasard"

TILE = {
    'correct_place': '🟩',
    'correct_letter': '🟨',
    'incorrect_letter': '⬛'
}
