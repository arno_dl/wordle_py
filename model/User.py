import json
import sys
sys.path.append('..')
from constants import SAVE_FILE

class User:
    """
    Utilisateur du programme.
    """
    def __init__(self, name, firstname, nickname, default_save=SAVE_FILE):
        self.name = name
        self.firstname = firstname
        self.nickname = nickname
        self.default_save = default_save # Peut être un attribut de classe ?
        self._get_save()

    def _get_save(self):
        """
        Méthode privée pour charger la sauvegarde d'un joueur depuis le fichier
        de sauvegarde par défaut.
        """
        save = {}
        try:
            with open(self.default_save, "r") as s:
                save = json.load(s)
        except (FileNotFoundError, json.JSONDecodeError):
            with open(self.default_save, "w") as s:
                s.write("{}")

        try:
            self.save = save[self.nickname]
        except KeyError:
            save[self.nickname] = {
                    "name": self.name,
                    "firstname": self.firstname,
                    "scores": [],
                    "words": [],
                    }
            self.save = save[self.nickname]

    def set_save(self):
        """
        Sauvegarde le score de l'utilisateur dans le fichier de sauvegarde par
        défaut.
        """
        all_saves = {}
        with open(self.default_save, 'r') as all_s:
            all_saves = json.load(all_s)

        all_saves[self.nickname] = self.save

        with open(self.default_save, 'w') as s:
            json.dump(all_saves, s)
