import json.decoder
from time import sleep
from pyparsing import null_debug_action

import requests
from rich.progress import BarColumn, TimeRemainingColumn, Progress

import config
import constants
import utils

import logging
import logging.config

logging.config.fileConfig(fname='logging.conf', disable_existing_loggers=False)

logger = logging.getLogger(__name__)

# Param de la request
with_def = False
min_long = 2


# méthode recupérant un mot au hasard depuis l'api dicolink
def get_word(word_size, conjugate_word):
    request_word_params = f"{constants.URL_WORD_REQUEST}?avecdef={with_def}&minlong={min_long}&maxlong={word_size}&verbeconjugue={conjugate_word}&api_key={config.api_key}"
    try:
        return requests.get(request_word_params)        
    except requests.exceptions.HTTPError as rer:
       logger.error("La recupération du mot mystere à échouée", rer)

# méthode renvoyant le mot en json
def get_word_json(response) -> str:
    if response is not None: 
        try:
            response_json = response.json()
            guess_word = response_json[0]["mot"]
            return guess_word
        except json.decoder.JSONDecodeError as der:
            logger.error("Une erreur est survenue lors du parsing json", der)
        response.raise_for_status()
        



# méthode recupérant une liste de 10 mot au hasard depuis l'api dicolink et l'enregistre dans un fichier
def get_words(word_size, conjugate_word) -> list:
    request_words_params = f"{constants.URL_WORDS_REQUEST}?avecdef={with_def}&minlong={min_long}&maxlong={word_size}&verbeconjugue={conjugate_word}&limite=10&api_key={config.api_key}"
    word_list = []
    # juste pour le fun
    progress = Progress(
        '[progress.description]{task.description}',
        BarColumn(),
        '[magenta]{task.completed} de {task.total} mots téléchargés',
        TimeRemainingColumn()
    )
    try:
        response = requests.get(request_words_params)
        response_json = response.json()
        try:
            with progress:
                task = progress.add_task("[red]Récupération de la liste de mots...", total=len(response_json))
                for element in response_json:
                    sleep(0.1)
                    word_list.append(element['mot'])
                    progress.update(task, advance=1)
        except json.decoder.JSONDecodeError as der:
            logger.error("Une erreur est survenue lors du parsing json", der)
            response.raise_for_status()
    except requests.exceptions.HTTPError as rer:
        logger.error("La recupération du mot mystere à échouée", rer)
    utils.save_list(word_list)






