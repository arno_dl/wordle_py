"""
Toutes les fonctions liés aux statistiques de jeu.
"""

import json
from constants import SAVE_FILE

def load_save(save=SAVE_FILE):
    """
    Charge le fichier de sauvegarde
    """
    all_user_data = {}
    with open(save, 'r') as u:
        all_user_data = json.load(u)

    return all_user_data


def get_all_scores(save=SAVE_FILE):
    """
    Récupère tous les scores des utilisateurs et les renvoie dans l'ordre
    décroissant par nombre de points, dans une liste de tuples.
    """
    all_user_data = load_save(save)

    scores = {}
    for user, val in all_user_data.items():
        scores[user] = sum(all_user_data[user]["scores"])

    return sorted(zip(scores.values(), scores.keys()), reverse=True)


def get_all_words(save=SAVE_FILE):
    """
    Récupère et compte les mots trouvés par les utilisateurs et les renvoie
    dans l'ordre décroissant par nombre de mots, dans une liste de tuples.
    """
    all_user_data = load_save(save)

    words = {}
    for user, val in all_user_data.items():
        words[user] = len(all_user_data[user]["words"])

    return sorted(zip(words.values(), words.keys()), reverse=True)
