import os
import micromenu
import wordRequests
import logging
import logging.config

logging.config.fileConfig(fname='logging.conf', disable_existing_loggers=False)

logger = logging.getLogger(__name__)

clearConsole = lambda: os.system('cls' if os.name in ('nt', 'dos') else 'clear')

# Affiche le premier menu jouer / regles / quitter
def select_action():
    menu = micromenu.Menu("WORDLE","Que voulez vous faire ? ",cycle=False,min_width=50)
    
    menu.add_function_item("Jouer à Wordle",lambda x: select_level(x), {'x':''})
    menu.add_function_item("Voir la regle du jeux",lambda x: print_rules(x),{'x':''})
    menu.add_divider()
    menu.show()
    
# Permet de choisir le nivbeau de difficulté"
def select_level(arg):
    
    menu = micromenu.Menu("WORDLE","Choisissez le niveau de difficulté ",cycle=False,min_width=50)
    clearConsole()
    menu.add_function_item("mot de 5 lettres", lambda x: launch_request(x),{'x': 5})
    menu.add_function_item("mot de 8 lettres", lambda x: launch_request(x),{'x': 8})
    menu.add_function_item("mot de 10 lettres", lambda x: launch_request(x),{'x': 10})
    menu.add_divider()
    menu.show()

# Lance la request correspondant au choix de niveau
def launch_request(level):
    logger.info(f"Le joueur à choisi le niveau : {level}")
    wordRequests.get_words(level, False)
    
def print_rules(arg):
    # Afficher les regles du jeu
    pass