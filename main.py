from asyncio import constants
import random
from typing_extensions import Self
import menu
import utils
import logging
import logging.config
import artWork
import compare
from rich import print

logging.config.fileConfig(fname='logging.conf', disable_existing_loggers=False)

logger = logging.getLogger(__name__)


def main():
    """_summary_
    """
  
    
    # Affichage de l'artWork
    print(utils.randomArtwork())

    # Affichage menu jeux
    menu.select_action()
   
        # Pioche un mot dans la liste et affiche autant de tile que de lettres dans le mot    
    guess_word = utils.pick_word()

    # Affiche des tiles noires en lieu et place des lettres du mot à deviner
    utils.hide_word(guess_word)    
    
    def play():
        GUESS_NUMBER = 10
        GUESS = 0    
        while GUESS < GUESS_NUMBER:
            GUESS += 1            
            print(f"{GUESS} tentative :right_arrow: ")
            response = input()
            while len(guess_word) != len(response):
                print(f":sad: Vous devez saisir un mot de {len(guess_word)} lettres ")
                response = input()            
            else: 
                eval = compare.compare(response,guess_word)
                if eval[0] ==  True:
                    print(f":smile: Bravo, vous avez gagné en {GUESS} coup(s)")
                    menu.select_action()
                else :
                    print(" ".join(eval[1].values()))


    play()




 
if __name__ == '__main__':
    main()
