from rich import console
import random

import logging
import artWork
import logging.config

logging.config.fileConfig(fname='logging.conf', disable_existing_loggers=False)

logger = logging.getLogger(__name__)


# enregistre la liste dans un fichier, je sais c'est null tout le monde peut le voir
def save_list(list):
    with open('./out/guessWord.txt', 'w') as temp_op:
        for word in list:
            temp_op.write("%s\n" % word)
            
# créé un objet liste avec les mots du fichier passé en argument
def read_list() -> list:
    word_list = []
    with open('./out/guessWord.txt', 'r') as temp_re:
        word_list = temp_re.read().split('\n')
        logger.info(word_list)
    return word_list

# pioche un mot au hasard dans la liste
def pick_word() -> str:
    word_list = read_list()
    word = word_list.pop(random.randrange(len(word_list)))
    logger.info(word)
    return word            

# Renvoie le mot caché par des tiles
def hide_word(word):
    print('⬛ ' * len(word))
    
# Affichage de l'artWork    
def randomArtwork():   
     artworks = [artWork.get_art1(),artWork.get_art2(),artWork.get_art3()]
     return random.choice(artworks)