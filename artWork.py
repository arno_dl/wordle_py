
def get_art1() :
    return """
      [blue]/ \  /|[red]/  _ \/  __\/  _ \/ \   /  __/  /  __\\  \//
      [blue]| |  ||[red]| / \||  \/|| | \|| |   |  \    |  \/| \  / 
      [blue]| |/\||[red]| \_/||    /| |_/|| |_/\|  /_   |  __/ / /  
      [blue]\_/  \|[red]\____/\_/\_\\____/\____/\____\   \_/   /_/   
"""

def get_art2():
    return """
                                                                           ,-.----.             
    [red]           .---.[blue]                              ,--,                     \    /  \            
    [red]          /. ./|[blue]                       ,---,,--.'|                     |   :    \           
    [red]      .--'.  ' ;[blue]   ,---.    __  ,-.  ,---.'||  | :                     |   |  .\ :          
    [red]     /__./ \ : |[blue]  '   ,'\ ,' ,'/ /|  |   | ::  : '                     .   :  |: |          
    [red] .--'.  '   \' .[blue] /   /   |'  | |' |  |   | ||  ' |      ,---.          |   |   \ :    .--,  
    [red]/___/ \ |    ' '[blue].   ; ,. :|  |   ,',--.__| |'  | |     /     \         |   : .   /  /_ ./|  
    [red];   \  \;      :[blue]'   | |: :'  :  / /   ,'   ||  | :    /    /  |        ;   | |\`-', ' , ' :  
    [red] \   ;  \`      [blue]|'   | .; :|  | ' .   '  /  |'  : |__ .    ' / |        |   | ;  /___/ \: |  
    [red]  .   \    .\  ;[blue]|   :    |;  : | '   ; |:  ||  | '.'|'   ;   /|        :   ' |   .  \  ' |  
    [red]   \   \   ' \ |[blue] \   \  / |  , ; |   | '/  ';  :    ;'   |  / |        :   : :    \  ;   :  
    [red]    :   '  |--\"[blue]   \`----'   ---'  |   :    :||  ,   / |   :    |        |   | :     \  \  ;  
    [red]     \   \ ;    [blue]                  \   \  /   ---\`-'   \   \  /         \`---'.|      :  \  \ 
    [red]      '---\"    [blue]                    \`----'              \`----'            \`---\`       \  ' ; 
                                                                                          \`--\`  


"""

def get_art3():
    return """
          _____                   _______                   _____                    _____                    _____            _____                            _____               ______            
         /\    \                 /::\    \                 /\    \                  /\    \                  /\    \          /\    \                          /\    \              |\    \         
        /::\____\               /::::\    \               /::\    \                /::\    \                /::\____\        /::\    \                        /::\    \             |:\____\        
       /:::/    /              /::::::\    \             /::::\    \              /::::\    \              /:::/    /       /::::\    \                      /::::\    \            |::|   |        
      /:::/   _/___           /::::::::\    \           /::::::\    \            /::::::\    \            /:::/    /       /::::::\    \                    /::::::\    \           |::|   |        
     /:::/   /\    \         /:::/~~\:::\    \         /:::/\:::\    \          /:::/\:::\    \          /:::/    /       /:::/\:::\    \                  /:::/\:::\    \          |::|   |        
    /:::/   /::\____\       /:::/    \:::\    \       /:::/__\:::\    \        /:::/  \:::\    \        /:::/    /       /:::/__\:::\    \                /:::/__\:::\    \         |::|   |        
   /:::/   /:::/    /      /:::/    / \:::\    \     /::::\   \:::\    \      /:::/    \:::\    \      /:::/    /       /::::\   \:::\    \              /::::\   \:::\    \        |::|   |        
  /:::/   /:::/   _/___   /:::/____/   \:::\____\   /::::::\   \:::\    \    /:::/    / \:::\    \    /:::/    /       /::::::\   \:::\    \            /::::::\   \:::\    \       |::|___|______  
 /:::/___/:::/   /\    \ |:::|    |     |:::|    | /:::/\:::\   \:::\____\  /:::/    /   \:::\ ___\  /:::/    /       /:::/\:::\   \:::\    \          /:::/\:::\   \:::\____\      /::::::::\    \ 
|:::|   /:::/   /::\____\|:::|____|     |:::|    |/:::/  \:::\   \:::|    |/:::/____/     \:::|    |/:::/____/       /:::/__\:::\   \:::\____\        /:::/  \:::\   \:::|    |    /::::::::::\____\
|:::|__/:::/   /:::/    / \:::\    \   /:::/    / \::/   |::::\  /:::|____|\:::\    \     /:::|____|\:::\    \       \:::\   \:::\   \::/    /        \::/    \:::\  /:::|____|   /:::/~~~~/~~      
 \:::\/:::/   /:::/    /   \:::\    \ /:::/    /   \/____|:::::\/:::/    /  \:::\    \   /:::/    /  \:::\    \       \:::\   \:::\   \/____/          \/_____/\:::\/:::/    /   /:::/    /         
  \::::::/   /:::/    /     \:::\    /:::/    /          |:::::::::/    /    \:::\    \ /:::/    /    \:::\    \       \:::\   \:::\    \                       \::::::/    /   /:::/    /          
   \::::/___/:::/    /       \:::\__/:::/    /           |::|\::::/    /      \:::\    /:::/    /      \:::\    \       \:::\   \:::\____\                       \::::/    /   /:::/    /           
    \:::\__/:::/    /         \::::::::/    /            |::| \::/____/        \:::\  /:::/    /        \:::\    \       \:::\   \::/    /                        \::/____/    \::/    /            
     \::::::::/    /           \::::::/    /             |::|  ~|               \:::\/:::/    /          \:::\    \       \:::\   \/____/                          ~~           \/____/             
      \::::::/    /             \::::/    /              |::|   |                \::::::/    /            \:::\    \       \:::\    \                                                               
       \::::/    /               \::/____/               \::|   |                 \::::/    /              \:::\____\       \:::\____\                                                              
        \::/____/                 ~~                      \:|   |                  \::/____/                \::/    /        \::/    /                                                              
         ~~                                                \|___|                   ~~                       \/____/          \/____/                                                               
                                                                                                                                                                                                    
"""

