import os
from model.User import User
from scores import *

os.system("rm test_save_file.json")
test_save_file = "./test_save_file.json"

################################
#            USERS             #
################################

u = User("Morane", "Bob", "Bobby", test_save_file)
u.save["scores"].append(3)
u.save["words"].append("test")
u.save["words"].append("test")
u.save["words"].append("test")
u.save["words"].append("test")
u.set_save()
print(f"OK - User {u.nickname} created")

u2 = User("Götz", "Von Berlichingen", "Arsche", test_save_file)
u2.set_save()
print(f"OK - User {u2.nickname} created")

u3 = User("Butchers", "Builders", "country", test_save_file)
u3.save["scores"].append(6)
u3.save["scores"].append(6)
u3.save["words"].append("test")
u3.save["words"].append("test")
u3.save["words"].append("test")
u3.set_save()
print(f"OK - User {u3.nickname} created")

u4 = User("Whine", "Bread", "Rome", test_save_file)
u4.save["scores"].append(7)
u4.save["scores"].append(3)
u4.save["words"].append("test")
u4.save["words"].append("test")
u4.save["words"].append("test")
u4.set_save()
print(f"OK - User {u4.nickname} created")

################################
#           SCORES             #
################################

print("Scores:\n")
print(get_all_scores(test_save_file))

print("Words:\n")
print(get_all_words(test_save_file))
